#!/bin/bash

#creation des dossiers et fichiers necessaires :

mkdir USB1
mkdir USB2
mkdir RAMDISK
mkdir DISK
touch DISK/database

#creation des fichiers password :

dd if=/dev/random bs=32 count=1 | hexdump -ve '1/1 "%02x"' > USB1/password
dd if=/dev/random bs=32 count=1 | hexdump -ve '1/1 "%02x"' > USB2/password

#mots de passe de protection des fichiers password

gpg -c USB1/password
gpg -c USB2/password



