#!/bin/bash

#creation des dossiers et fichiers necessaires :

mkdir USB_T1
mkdir USB_J1
#creation des fichiers password :

dd if=/dev/random bs=32 count=1 | hexdump -ve '1/1 "%02x"' > USB_T1/password
dd if=/dev/random bs=32 count=1 | hexdump -ve '1/1 "%02x"' > USB_J1/password

#mots de passe de protection des fichiers password

gpg -c USB_T1/password
gpg -c USB_J1/password



