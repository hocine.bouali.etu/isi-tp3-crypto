#!/bin/bash

openssl enc -aes-256-ecb -in RAMDISK/database -out RAMDISK/databasetmp.crypt -K $(cat USB1/password)
openssl enc -aes-256-ecb -in RAMDISK/databasetmp.crypt -out DISK/database.crypt -K $(cat USB2/password)
rm RAMDISK/databasetmp.crypt
rm RAMDISK/database


rm USB1/password
rm USB2/password


