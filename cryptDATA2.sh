#!/bin/bash

key1=$(cat $1/password)
key2=$(cat $2/password)

openssl enc -aes-256-ecb -in RAMDISK/database -out RAMDISK/databasetmp.crypt -K $key1
openssl enc -aes-256-ecb -in RAMDISK/databasetmp.crypt -out DISK/database.crypt -K $key2
rm RAMDISK/databasetmp.crypt
rm RAMDISK/database

rm USB_J1/password
rm USB_T1/password

rm USB1/password
rm USB2/password

