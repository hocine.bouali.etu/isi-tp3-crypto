# Auteur

MEGHARI Samy

BOUALI Hocine


## Question 1 :


La clé de cryptage principale, MASTER_KEY, est cryprté et stocké sur le serveur par la clé KEY_M.

Chaque utilisateur à une clé USB sur laquelle est stocké une partie de KEY_M. Le fichier contenant cette partie est également crypté avec une clé, KEY_R connu par le serveur.

Lorsque l'on veut récupérer la MASTER_KEY les deux responsables doivent alors insérer leurs USB puis donner leur mot de passe.S'il est correcte alors on décrypte grace a KEY_R la partie de la clé KEY_M présente sur la clé USB.

Puis en concaténant les deux parties on obtient la clé KEY_M qui est capable de décrypter la MASTER_KEY.

## Question 2 : 

On lance init.sh pour créer les dossiers et fichiers nécessaires ainsi que les mots de pase.

On a ensuite les services suivants :

addpair.sh nom numéro : ajouter un paire nom numéro

removepair.sh nom numéro : supprime une paire

findpair.sh nom numéro : trouve une paire

cryptDATA.sh : crypte la database

decryptDATA.sh : décrypte la database

## Question 3 :

Pour cette problématique les responsables auront la même clés que les representant mais avec des usb aux mots de passe différents.

## Question 4 :

init2.sh : ajouter les nouvelles USB et leurs mots de passe.

cryptDATA2.sh USB_T1 USB_T2 : crypte avec les USB des representants.

decryptDATA2.sh USB_T1 USB_T2 : decrypte avec les USB des representants0

## Question 5 :

Pour la répudation l'USB retirée sera remplacée par une nouvelle.

Cette dernière sera utilisée pour crypter la database.

## Question 6 : 

La database doit être décryptée.

repudiation.sh USB_RESPONSABLE USB_REPRESENTANT : modifie le contenu des clés

on doit a nouveau mettre des mots de passe aux clés

gpg -c USB_RESPONSABLE/password

gpg -c USB_REPRESENTANT/password

on peut maintenant recrypté avec la nouvelle clé